<?php

namespace App\Http\Controllers;

use App\Imports\SalarySurveyImport;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class FrontController extends Controller
{
    public function importFromCsv()
    {
        Excel::import(new SalarySurveyImport(), public_path('csv/salary.csv'));

        return redirect('/')->with('success', 'All good!');
    }
}
