<?php

namespace App\Http\Livewire;

use Livewire\Component;

class SalarySurvey extends Component
{

    public $years_of_experience;
    public $year;
    public $salary_surveys;

    public function mount()
    {
        if (!is_null($this->year)) {
            $this->salary_surveys = \App\Models\SalarySurvey::where('years_of_experience', $this->year)->get();
        } else {
            $this->salary_surveys = \App\Models\SalarySurvey::all();
        }
    }

    public function render()
    {
        $this->years_of_experience = \App\Models\SalarySurvey::select('years_of_experience')
            ->groupBy('years_of_experience')
            ->orderBy('years_of_experience')
            ->get();


        return view('livewire.salary-survey', [
            "years_of_experience" => $this->years_of_experience
        ]);
    }


}
