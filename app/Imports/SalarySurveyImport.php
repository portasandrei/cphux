<?php

namespace App\Imports;

use App\Models\SalarySurvey;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class SalarySurveyImport implements ToModel, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new SalarySurvey([
            'timestamp'     => $row['timestamp'],
            'permission'    => $row['permission'],
            'gender'        => $row['gender'],
            'postal_code'    => $row['postal_code'],
            'education'    => $row['education'],
            'education_institution'    => $row['education_institution'],
            'years_of_experience'    => $row['years_of_experience'],
            'employment_commitment'    => $row['employment_commitment'],
            'employment_type'    => $row['employment_type'],
            'job_category'    => $row['job_category'],
            'monthly_salary'    => $row['monthly_salary'],
            'job_title'    => $row['job_title'],
        ]);
    }

    public function headingRow(): int{
        return 1;
    }

}
