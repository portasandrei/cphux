<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SalarySurvey extends Model
{
    use HasFactory;

    protected $fillable = [
        'timestamp',
        'permission',
        'gender',
        'postal_code',
        'education',
        'education_institution',
        'years_of_experience',
        'employment_commitment',
        'employment_type',
        'job_category',
        'monthly_salary',
        'job_title',
    ];
}
