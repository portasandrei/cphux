<div>
    <label for="year">Choose year</label>
    <select wire:model="year">
        <option>Choose years of experience</option>
        @foreach($years_of_experience as $year_of_experience)
            <option value="{{ $year_of_experience->years_of_experience }}">{{ $year_of_experience->years_of_experience }}</option>
        @endforeach
    </select>
</div>
@json($year)
<div>
    <table class="table-auto">
        <tr>
            <th>Timestamp</th>
            <th>Permission</th>
            <th>Gender</th>
            <th>Postal Code</th>
            <th>Education</th>
            <th>Education Institution</th>
            <th>Years of Experience</th>
            <th>Employment Commitment</th>
            <th>Employment Type</th>
            <th>Job Category</th>
            <th>Monthly Salary</th>
            <th>Job Title</th>
        </tr>

        @if (!empty($salary_surveys))
            @foreach($salary_surveys as $salary_survey)
                <tr>
                    <td>
                        {{ $salary_survey->timestamp }}
                    </td>
                    <td>
                        {{ $salary_survey->permission }}
                    </td>
                    <td>
                        {{ $salary_survey->gender }}
                    </td>
                    <td>
                        {{ $salary_survey->postal_code }}
                    </td>
                    <td>
                        {{ $salary_survey->education }}
                    </td>
                    <td>
                        {{ $salary_survey->education_institution }}
                    </td>
                    <td>
                        {{ $salary_survey->years_of_experience }}
                    </td>
                    <td>
                        {{ $salary_survey->employment_commitment }}
                    </td>
                    <td>
                        {{ $salary_survey->employment_type }}
                    </td>
                    <td>
                        {{ $salary_survey->job_category }}
                    </td>
                    <td>
                        {{ $salary_survey->monthly_salary }}
                    </td>
                    <td>
                        {{ $salary_survey->job_title }}
                    </td>
                </tr>
            @endforeach
        @else
            No results
        @endif
    </table>
</div>

