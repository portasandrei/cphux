<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
{{--        <link href="{{ resource_path('css/app.css') }}" rel="stylesheet">--}}
        @vite('resources/css/app.css')
        <script src="https://cdn.tailwindcss.com/?plugins=forms"></script>
        @livewireStyles
    </head>
    <body>
        <livewire:salary-survey />
        @livewireScripts
    </body>
</html>
